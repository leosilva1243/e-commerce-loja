import * as React from 'react'
import { History } from 'history'
import { Carrinho } from '../type/carrinho'

import { buscarCarrinho, removerItem } from '../service/carrinho'

type CarrinhoProps = {
    history: History
}

type CarrinhoState = {
    carrinho: Carrinho | null
}

class Component extends React.Component<CarrinhoProps, CarrinhoState> {
    state: CarrinhoState = {
        carrinho: null
    }

    async componentDidMount() {
        const carrinho = await buscarCarrinho()
        console.log(carrinho)
        this.setState({ carrinho })
    }

    render() {
        return (
            <div className="container">
                <h1>Carrinho</h1>
                <hr />
                <div className="row">
                    <div className="col-xs-6">
                        <h3>Produtos</h3>
                        {this.state.carrinho && (
                            <List>
                                {this.state.carrinho.itens.length === 0
                                    ? <h3>Seu carrinho está vazio.</h3>
                                    : this.state.carrinho.itens.map(item => (
                                        <ListItem
                                            key={item.produto.id}
                                            nome={item.produto.identificacao}
                                            quantidade={item.quantidade}
                                            onDelete={async () => {
                                                const carrinho = await removerItem(item.produto.id)
                                                this.setState({ carrinho })
                                            }}
                                        />
                                    ))}
                            </List>
                        )}
                    </div>
                </div>
                <button
                    className="btn btn-primary"
                    onClick={() => {
                        this.props.history.push('/')
                    }}
                >
                    Continuar comprando
                </button>
            </div>
        )
    }
}

function List(props: { children: React.ReactNode }) {
    return (
        <ul className="list-group">
            {props.children}
        </ul>
    )
}

function ListItem(props: { nome: string, quantidade: number, onDelete: () => void }) {
    return (
        <li className="list-group-item" style={{ display: 'flex' }}>
            <div style={{ flex: 1, fontSize: '20px', padding: '4px' }}>
                {props.nome} ({props.quantidade})
            </div>
            <div>
                <button
                    className="btn btn-danger"
                    onClick={() => {
                        props.onDelete()
                    }}
                >
                    <span className="glyphicon glyphicon-remove-sign" />
                </button>
            </div>
        </li>
    )
}

export default Component