import * as React from 'react'
import { History } from 'history'

import { autenticar } from '../service/conta'

type AutenticarProps = {
    history: History
}

type AutenticarState = {
    email: string
    password: string
    error: string | null
}

class Autenticar extends React.Component<AutenticarProps, AutenticarState> {
    state: AutenticarState = {
        email: '',
        password: '',
        error: null
    }


    render() {
        return (
            <form>
                <div className="container">
                    <div className="row">
                        <div className="col-xs-4 col-xs-offset-4">
                            <h3>Autenticar</h3>
                            <br />
                            <div className="form-group">
                                <label>Email</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    value={this.state.email}
                                    onChange={event => {
                                        const email = event.target.value
                                        this.setState({ email })
                                    }}
                                />
                            </div>
                            <div className="form-group">
                                <label>Senha</label>
                                <input
                                    type="password"
                                    className="form-control"
                                    value={this.state.password}
                                    onChange={event => {
                                        const password = event.target.value
                                        this.setState({ password })
                                    }}
                                />
                            </div>
                            {!!this.state.error && (
                                <div>
                                    <label className="text-danger">{this.state.error}</label>
                                    <br /><br />
                                </div>
                            )}
                            <button
                                type="submit"
                                className="btn btn-primary"
                                onClick={this.autenticar}
                            >
                                Autenticar
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        )
    }

    private autenticar = async (event: any) => {
        event.preventDefault()

        try {
            await autenticar(this.state.email, this.state.password)
            this.setState({ error: null })
            this.props.history.push('/')
        } catch (error) {
            this.setState({ error: 'Não foi possível autenticar-se. Algo deu errado.' })
        }
    }
}

export default Autenticar