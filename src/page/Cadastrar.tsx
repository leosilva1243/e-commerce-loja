import * as React from 'react'
import { History } from 'history'

import { criar } from '../service/conta'

type CadastrarProps = {
    history: History
}

type CadastrarState = {
    email: string
    password: string
    repeatPassword: string
    error: string | null
    sexo: string
    cpf: string
    nome: string
}

class Cadastrar extends React.Component<CadastrarProps, CadastrarState> {
    state: CadastrarState = {
        email: '',
        password: '',
        repeatPassword: '',
        error: null,
        sexo: '',
        cpf: '',
        nome: ''
    }

    render() {
        return (
            <form>
                <div className="container">
                    <div className="row">
                        <div className="col-xs-4 col-xs-offset-4">
                            <h3>Cadastrar</h3>
                            <br />
                            <div className="form-group">
                                <label>Nome</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    value={this.state.nome}
                                    onChange={event => {
                                        const nome = event.target.value
                                        this.setState({ nome })
                                    }}
                                />
                            </div>
                            <div className="form-group">
                                <label>Email</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    value={this.state.email}
                                    onChange={event => {
                                        const email = event.target.value
                                        this.setState({ email })
                                    }}
                                />
                            </div>
                            <div className="form-group">
                                <label>CPF</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    value={this.state.cpf}
                                    onChange={event => {
                                        const cpf = event.target.value
                                        this.setState({ cpf })
                                    }}
                                />
                            </div>
                            <div className="form-group">
                                <label>Sexo</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    value={this.state.sexo}
                                    onChange={event => {
                                        const sexo = event.target.value
                                        this.setState({ sexo })
                                    }}
                                />
                            </div>
                            <hr />
                            <div className="form-group">
                                <label>Senha</label>
                                <input
                                    type="password"
                                    className="form-control"
                                    value={this.state.password}
                                    onChange={event => {
                                        const password = event.target.value
                                        this.setState({ password })
                                    }}
                                />
                            </div>
                            <div className="form-group">
                                <label>Repetir senha</label>
                                <input
                                    type="password"
                                    className="form-control"
                                    value={this.state.repeatPassword}
                                    onChange={event => {
                                        const repeatPassword = event.target.value
                                        this.setState({ repeatPassword })
                                    }}
                                />
                            </div>
                            {!!this.state.error && (
                                <div>
                                    <label className="text-danger">{this.state.error}</label>
                                    <br /><br />
                                </div>
                            )}
                            <button
                                type="submit"
                                className="btn btn-primary"
                                onClick={this.cadastrar}
                            >
                                Cadastrar
                        </button>
                        </div>
                    </div>
                </div>
            </form>
        )
    }

    private cadastrar = async (event: any) => {
        event.preventDefault()

        if (!this.state.nome) {
            this.setState({ error: 'O campo nome deve ser preenchido.' })
            return
        }
        if (!this.state.cpf) {
            this.setState({ error: 'O campo CPF deve ser preenchido.' })
            return
        }
        if (!this.state.sexo) {
            this.setState({ error: 'O campo sexo deve ser preenchido.' })
            return
        }
        if (!this.state.email) {
            this.setState({ error: 'O campo email deve ser preenchido.' })
            return
        }
        if (!this.state.password) {
            this.setState({ error: 'O campo senha deve ser preenchido' })
            return
        }
        if (this.state.password !== this.state.repeatPassword) {
            this.setState({ error: 'Os campos senha e repetir senha devem conter o mesmo valor.' })
            return
        }

        try {
            await criar(
                this.state.email,
                this.state.password,
                this.state.nome,
                this.state.cpf,
                this.state.sexo
            )
            this.setState({ error: null })
            this.props.history.push('/autenticar')
        } catch (error) {
            this.setState({ error: 'Não foi possível criar a conta. Algo deu errado no servidor.' })
        }
    }
}

export default Cadastrar