import * as React from 'react'
import { ChangeEvent } from 'react'
import { RouteComponentProps } from 'react-router-dom'

import PrecoFormatter from '../component/PrecoFormatter'
import TamanhoFormatter from '../component/TamanhoFormatter'
import PesoFormatter from '../component/PesoFormatter'
import { ProdutoService } from '../service/produto'
import { Produto } from '../type/produto'
import calcularFrete, { Frete } from '../calcular-frete'
import { adicionarItem } from '../service/carrinho'

interface ProdutoPageParams {
    id: number
}

interface ProdutoPageProps extends RouteComponentProps<Partial<ProdutoPageParams>> {
    service: ProdutoService
}

interface ProdutoPageState {
    produto: Produto | undefined
    cep: string
    frete: { sedex: Frete, pac: Frete } | undefined,
    quantidade: number
}

class ProdutoPage extends React.Component<ProdutoPageProps, ProdutoPageState> {
    state: ProdutoPageState = {
        produto: undefined,
        cep: '',
        frete: undefined,
        quantidade: 1
    }

    constructor() {
        super()
        this.atualizarCep = this.atualizarCep.bind(this)
    }

    componentWillMount() {
        this.carregarProduto()
    }

    render(): JSX.Element {
        const produto = this.state.produto
        const frete = this.state.frete
        return (
            <div className="container">
                {produto === undefined ?
                    <h1>Produto não encontrado</h1> : (
                        <div className="row">
                            <div className="col-md-5">
                                <img src="http://placehold.it/400x400" alt="" />
                            </div>
                            <div className="col-md-7">
                                <h1>{produto.identificacao}</h1>
                                <div className="ratings">
                                    <p>
                                        <span className="glyphicon glyphicon-star" />
                                        <span className="glyphicon glyphicon-star" />
                                        <span className="glyphicon glyphicon-star" />
                                        <span className="glyphicon glyphicon-star" />
                                        <span className="glyphicon glyphicon-star" />
                                        <span> (5)</span>
                                    </p>
                                </div>
                                <hr />
                                <h3>Valor</h3>
                                <div className="row">
                                    <div className="col-md-6">
                                        <h4>
                                            <PrecoFormatter preco={produto.preco} />
                                        </h4>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="form-group">
                                            <label>Quantidade: </label>
                                            <input
                                                type="number"
                                                className="form-control"
                                                value={this.state.quantidade}
                                                onChange={(event) => {
                                                    this.setState({ quantidade: Number(event.target.value) })
                                                }}
                                            />
                                        </div>
                                        <br />
                                        <button
                                            className="btn btn-primary btn-block btn-ld"
                                            onClick={async () => {
                                                await adicionarItem(
                                                    this.state.produto!.id,
                                                    this.state.quantidade
                                                )
                                                this.props.history.push('/carrinho')
                                            }}
                                        >
                                            Adicionar ao carrinho
                                        </button>
                                    </div>
                                </div>
                                <hr />
                                <h3>Calcular frete</h3>
                                <div className="row">
                                    <div className="col-md-6">
                                        <h4 style={{ marginBottom: '16px' }}>CEP:</h4>
                                        <input type="text" className="form-control" id="cep" name="cep"
                                            placeholder="CEP" value={this.state.cep} onChange={this.atualizarCep} />
                                    </div>
                                    <div className="col-md-6">
                                        {frete !== undefined && (
                                            <div className="row">
                                                <div className="col-xs-6">
                                                    <ResultadoFrete frete={frete.sedex} nome="Sedex" />
                                                </div>
                                                <div className="col-xs-6">
                                                    <ResultadoFrete frete={frete.pac} nome="PAC" />
                                                </div>
                                            </div>)}
                                    </div>
                                </div>
                                <hr />
                                <h3>Detalhes</h3>
                                <strong>Descrição: </strong>{produto.descricao}<br />
                                <strong>Fabricante: </strong>{produto.fabricante}<br />
                                <strong>Peso: </strong><PesoFormatter peso={produto.peso} /><br />
                                <strong>Dimensões: </strong><TamanhoFormatter tamanho={produto.dimensoes} /><br />
                            </div>
                        </div>)}
            </div>
        )
    }

    private async atualizarCep(event: ChangeEvent<HTMLInputElement>) {
        event.preventDefault()
        const cep = event.target.value
        if (!cep.match(/^[0-9]*$/)) {
            return
        }
        this.setState({ cep })
        if (cep.match(/^[0-9]{8}$/)) {
            const produto = this.state.produto!
            const freteOrigem = '01505010'
            const frete = await calcularFrete(freteOrigem, cep, produto)
            this.setState({ frete })
        }
        else {
            this.setState({ frete: undefined })
        }
    }

    private async carregarProduto() {
        const id = this.props.match.params.id
        if (id === undefined) { return }
        try {
            const produto = await this.props.service.find(id)
            this.setState({ produto })
        } catch (error) {
            alert('não foi possível carregar o produto!')
        }
    }
}

function ResultadoFrete(props: { nome: string, frete: Frete }) {
    return (
        <div>
            <h4>{props.nome}</h4>
            <strong>Valor: </strong> <PrecoFormatter preco={props.frete.valor} /> <br />
            <strong>Prazo </strong>{props.frete.prazo} dias úteis
    </div >
    )
}

export default ProdutoPage