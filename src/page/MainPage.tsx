import * as React from 'react'
import { Link } from 'react-router-dom'

import './MainPage.css'
import PrecoFormatter from '../component/PrecoFormatter'
import Sidebar from '../component/Sidebar'
import { ProdutoService } from '../service/produto'
import { Produto } from '../type/produto'

interface MainPageProps {
    service: ProdutoService
}

interface MainPageState {
    produtos: Produto[]
}

class MainPage extends React.Component<MainPageProps, MainPageState> {
    state: MainPageState = {
        produtos: []
    }

    componentWillMount() {
        this.carregarProdutos()
    }

    render(): JSX.Element {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-3">
                        <Sidebar />
                    </div>
                    <div className="col-md-9">
                        <div className="row carousel-holder">
                            <div className="col-md-12">
                                <div id="carousel-example-generic" className="carousel slide" data-ride="carousel">
                                    <ol className="carousel-indicators">
                                        <li data-target="#carousel-example-generic" 
                                        data-slide-to="0" className="active" />
                                        <li data-target="#carousel-example-generic" 
                                        data-slide-to="1" />
                                        <li data-target="#carousel-example-generic" 
                                        data-slide-to="2" />
                                    </ol>
                                    <div className="carousel-inner">
                                        <div className="item active">
                                            <img className="slide-image" src="http://placehold.it/800x300" alt="" />
                                        </div>
                                        <div className="item">
                                            <img className="slide-image" src="http://placehold.it/800x300" alt="" />
                                        </div>
                                        <div className="item">
                                            <img className="slide-image" src="http://placehold.it/800x300" alt="" />
                                        </div>
                                    </div>
                                    <a className="left carousel-control" 
                                    href="#carousel-example-generic" data-slide="prev">
                                        <span className="glyphicon glyphicon-chevron-left" />
                                    </a>
                                    <a className="right carousel-control" 
                                    href="#carousel-example-generic" data-slide="next">
                                        <span className="glyphicon glyphicon-chevron-right" />
                                    </a>
                                </div>
                            </div>

                        </div>

                        <div className="row">
                            {this.state.produtos.map(produto => {
                                return <ProdutoItem key={produto.id} produto={produto}/>
                            })}
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    private async carregarProdutos() {
        const produtos = await this.props.service.list()
        this.setState({ produtos })
    }
}

interface ProdutoItemProps {
    produto: Produto
}

function ProdutoItem(props: ProdutoItemProps): JSX.Element {
    return (
        <div className="col-sm-4 col-lg-4 col-md-4">
            <Link to={`/produtos/${props.produto.id}`}>
                <div className="thumbnail">
                    <img src="http://placehold.it/320x150" alt="" />
                    <div className="caption">
                        <h4 className="pull-right">
                            <PrecoFormatter preco={props.produto.preco} />
                        </h4>
                        <h4><a href="#">{props.produto.identificacao}</a>
                        </h4>
                        <p>{props.produto.descricao}</p>
                    </div>
                    <div className="ratings">
                        <p className="pull-right">15 revisões</p>
                        <p>
                            <span className="glyphicon glyphicon-star" />
                            <span className="glyphicon glyphicon-star" />
                            <span className="glyphicon glyphicon-star" />
                            <span className="glyphicon glyphicon-star" />
                            <span className="glyphicon glyphicon-star" />
                        </p>
                    </div>
                </div>
            </Link>
        </div>
    )
}

export default MainPage