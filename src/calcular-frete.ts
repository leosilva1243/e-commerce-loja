import * as xml from 'xml2js'
import axios from 'axios'
import * as qs from 'qs'
import { Produto } from './type/produto';

const url = 'http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx'
const parser = new xml.Parser({ async: false, attrkey: '@', explicitArray: false })

const SEDEX = 40010
const PAC = 41106

export type Frete = {
    valor: number,
    prazo: number
}

const calcularFrete: (
    cepOrigem: string, cepDestino: string, produto: Produto
) => Promise<{ sedex: Frete, pac: Frete }> = async (
    cepOrigem, cepDestino, produto
) => {
        const params = {
            sCepOrigem: cepOrigem,
            sCepDestino: cepDestino,
            nVlPeso: produto.peso,
            nVlComprimento: Math.max(16, produto.dimensoes.profundidade),
            nVlAltura: Math.max(2, produto.dimensoes.altura),
            nVlLargura: Math.max(11, produto.dimensoes.largura),
            nVlValorDeclarado: produto.preco,

            nCdFormato: '1',
            nVlDiametro: 0,
            sCdMaoPropria: 's',
            sCdAvisoRecebimento: 'n',
            StrRetorno: 'xml',
            nCdServico: `${SEDEX},${PAC}`
        }

        const query = qs.stringify(params)
        const response = await axios.get(url + '?' + query)
        const resposta = await new Promise((resolve, reject) => {
            parser.parseString(response.data, (error?: Error, object?: any) => {
                if (error) {
                    reject(error)
                } else {
                    resolve(object)
                }
            })
        })
        const servicos = resposta['Servicos']['cServico']
        const sedex = servicos[0]
        const pac = servicos[1]
        return {
            sedex: {
                prazo: Number(sedex['PrazoEntrega']),
                valor: Number(sedex['Valor'].replace(',', '.'))
            },
            pac: {
                prazo: Number(pac['PrazoEntrega']),
                valor: Number(pac['Valor'].replace(',', '.'))
            }
        }
    }

export default calcularFrete