
export type CarrinhoItem = {
    produto: {
        id: number
        identificacao: string
    }
    quantidade: number
}

export type Carrinho = {
    itens: CarrinhoItem[]
}