
export interface Tamanho {
    altura: number
    largura: number
    profundidade: number
}