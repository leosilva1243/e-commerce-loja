import { Tamanho } from './tamanho'

export interface Produto {
    id: number
    identificacao: string
    categoria: string
    fabricante: string
    preco: number
    peso: number
    dimensoes: Tamanho
    descricao: string
}