import * as React from 'react'

class Footer extends React.Component<{}, null> {
    render(): JSX.Element {
        return (
            <div className="container">
                <hr />
                <footer>
                    <div className="row">
                        <div className="col-lg-12">
                            <p>E-commerce 2017</p>
                        </div>
                    </div>
                </footer>
            </div>
        )
    }
}

export default Footer