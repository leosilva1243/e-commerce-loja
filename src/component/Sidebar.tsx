import * as React from 'react'

function Sidebar(props: {}): JSX.Element {
    return (
        <div>
            <p className="lead">E-commerce</p>
            <div className="list-group">
                <a href="#" className="list-group-item">Categoria 1</a>
                <a href="#" className="list-group-item">Categoria 2</a>
                <a href="#" className="list-group-item">Categoria 3</a>
            </div>
        </div>
    )
}

export default Sidebar