import * as React from 'react'

import NumeroFormatter from './NumeroFormatter'

interface PrecoFormatterProps {
    preco: number
}

function PrecoFormatter(props: PrecoFormatterProps): JSX.Element {
    return <span>R$ <NumeroFormatter valor={props.preco}/></span>
}

export default PrecoFormatter