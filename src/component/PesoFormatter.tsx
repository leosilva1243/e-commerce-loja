import * as React from 'react'

import NumeroFormatter from './NumeroFormatter'

interface PesoFormatterProps {
    peso: number
}

function PesoFormatter(props: PesoFormatterProps): JSX.Element {
    return (
        <span><NumeroFormatter valor={props.peso} />g</span>
    )
}

export default PesoFormatter