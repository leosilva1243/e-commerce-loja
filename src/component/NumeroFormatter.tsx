import * as React from 'react'

interface NumeroFormatterProps {
    valor: number
}

function NumeroFormatter(props: NumeroFormatterProps): JSX.Element {
    return <span>{props.valor.toFixed(2).replace('.', ',')}</span>
}

export default NumeroFormatter