import * as React from 'react'

import NumeroFormatter from './NumeroFormatter'
import { Tamanho } from '../type/tamanho'

interface TamanhoProps {
    tamanho: Tamanho
}   

function TamanhoFormatter(props: TamanhoProps): JSX.Element {
    const tamanho = props.tamanho
    return (
        <span>
            A: <NumeroFormatter valor={tamanho.altura}/>cm;
            L: <NumeroFormatter valor={tamanho.largura}/>cm; 
            P: <NumeroFormatter valor={tamanho.profundidade}/>cm
        </span>
    )
}

export default TamanhoFormatter