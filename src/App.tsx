import * as React from 'react'
import { BrowserRouter, Route, Redirect } from 'react-router-dom'
import './App.css'

import Navbar from './Navbar'
import MainPage from './page/MainPage'
import ProdutoPage from './page/ProdutoPage'
import Carrinho from './page/Carrinho'
import Cadastrar from './page/Cadastrar'
import Autenticar from './page/Autenticar'
import Footer from './Footer'
import { ProdutoService } from './service/produto'

const produtoService = new ProdutoService()

class App extends React.Component<{}, null> {
  render() {
    return (
      <BrowserRouter>
        <div className="App">
          <Navbar />
          <Route exact={true} path="/" render={() => <MainPage service={produtoService} />} />
          <Route exact={true} path="/produtos" render={() => <Redirect to="/" />} />
          <Route exact={true} path="/produtos/:id"
            render={props => <ProdutoPage {...props} service={produtoService} />} />
          <Route exact={true} path="/carrinho"
            render={({ history }) => <Carrinho history={history} />}
          />
          <Route exact={true} path="/cadastrar"
            render={({ history }) => <Cadastrar history={history} />}
          />
          <Route exact={true} path="/autenticar"
            render={({ history }) => <Autenticar history={history} />}
          />
          <Footer />
        </div>
      </BrowserRouter>
    )
  }
}

export default App
