import * as React from 'react'
import { Link } from 'react-router-dom'

class Navbar extends React.Component<{}, null> {
    render(): JSX.Element {
        return (
            <nav className="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <div className="container">
                    <div className="navbar-header">
                        <button type="button" className="navbar-toggle" 
                        data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span className="sr-only">Toggle navigation</span>
                            <span className="icon-bar" />
                            <span className="icon-bar" />
                            <span className="icon-bar" />
                        </button>
                        <Link className="navbar-brand" to="/">E-commerce</Link>
                    </div>
                    <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul className="nav navbar-nav">
                            <li>
                                <Link to="/">Sobre</Link>
                            </li>
                            <li>
                                <Link to="/">Contatos</Link>
                            </li>
                            <li>
                                <Link to="/carrinho">Carrinho</Link>
                            </li>
                            <li className="pull-right">
                                <Link to="/cadastrar">Cadastrar</Link>
                            </li>
                            <li className="pull-right">
                                <Link to="/autenticar">Autenticar</Link>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        )
    }
}

export default Navbar