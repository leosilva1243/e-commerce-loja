import { url as api } from './config'
import axios from 'axios'

const url = api + '/conta'

export async function criar(email: string, senha: string, nome: string, cpf: string, sexo: string) {
    const data = { email, senha, nome, cpf, sexo }
    await axios.post(url + '/criar', data)
}

export async function autenticar(email: string, senha: string): Promise<string> {
    const data = { email, senha }
    const response = await axios.post(url + '/autenticar', data)
    return response.data // TOKEN
}