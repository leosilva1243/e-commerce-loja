import axios from 'axios'

import { Produto } from '../type/produto'

export class ProdutoService {
    private readonly url = 'http://localhost:8080/produtos'

    async find(id: number): Promise<Produto | undefined> {
        return axios.get(`${this.url}/${id}`)
            .then(response => response.data as Produto)
    }

    async list(): Promise<Produto[]> {
        return axios.get(this.url)
            .then(response => response.data as Produto[])
    }

    async add(entity: Produto): Promise<Produto> {
        return axios.post(this.url, entity)
            .then(response => response.data as Produto)
    }

    async update(entity: Produto): Promise<void> {
        return axios.put(`${this.url}/${entity.id}`, entity)
            .then(() => undefined)
    }

    async delete(id: number): Promise<Produto> {
        return axios.delete(`${this.url}/${id}`)
            .then(response => response.data as Produto)
    }
}