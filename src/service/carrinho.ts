import { url as api } from './config'
import axios from 'axios'

const url = api + '/carrinho'

export async function adicionarItem(produtoId: number, quantidade: number) {
    const response = await axios.post(
        url + '/adicionar-item',
        { produtoId, quantidade }
    )
    return response.data
}

export async function removerItem(produtoId: number) {
    const response = await axios.post(
        url + '/remover-item',
        { produtoId }
    )
    return response.data
}

export async function limparCarrinho() {
    const response = await axios.post(
        url + '/limpar-carrinho'
    )
    return response.data
}

export async function buscarCarrinho() {
    const response = await axios.get(url)
    return response.data
}